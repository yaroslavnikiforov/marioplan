import React, { Component } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { signUp } from "../../../store/actions/authActions";

class SignUp extends Component {
  static propTypes = {
    auth: PropTypes.object,
    authError: PropTypes.object
  };

  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: ""
  };

  _handleChange = event =>
    this.setState({
      [event.target.id]: event.target.value
    });

  _handleSubmit = e => {
    e.preventDefault();

    this.props.signUp(this.state);
  };

  render() {
    const { auth, authError } = this.props;

    if (!auth.isEmpty) return <Redirect to="/" />;

    return (
      <div className="container">
        <form className="white" onSubmit={this._handleSubmit}>
          <h5 className="grey-text text-darken-3">Sign up</h5>
          <div className="input-field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={this._handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              onChange={this._handleChange}
              autoComplete="new-password"
            />
          </div>
          <div className="input-field">
            <label htmlFor="firstName">Firstname</label>
            <input type="text" id="firstName" onChange={this._handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="lastName">Lastname</label>
            <input type="text" id="lastName" onChange={this._handleChange} />
          </div>
          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Sign up</button>
            <div className="red-text center">
              {authError ? <p>{authError}</p> : null}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth,
  authError: state.auth.authError
});

export default connect(
  mapStateToProps,
  { signUp }
)(SignUp);
