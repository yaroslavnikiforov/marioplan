import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { createProject } from "../../../store/actions/projectActions";

class CreateProject extends Component {
  static propTypes = {
    auth: PropTypes.object,
    createProject: PropTypes.func
  };

  state = {
    title: "",
    content: ""
  };

  _handleChange = event =>
    this.setState({
      [event.target.id]: event.target.value
    });

  _handleSubmit = e => {
    e.preventDefault();

    this.props.createProject(this.state);
    this.props.history.push("/");
  };

  render() {
    const { auth } = this.props;

    if (auth.isEmpty) return <Redirect to="/signIn" />;

    return (
      <div className="container">
        <form className="white" onSubmit={this._handleSubmit}>
          <h5 className="grey-text text-darken-3">Create new project</h5>
          <div className="input-field">
            <label htmlFor="title">Title</label>
            <input type="text" id="title" onChange={this._handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="content">Project Content</label>
            <textarea
              id="content"
              className="materialize-textarea"
              onChange={this._handleChange}
            />
          </div>
          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Create</button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth
});

export default connect(
  mapStateToProps,
  { createProject }
)(CreateProject);
