import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

import { signOut } from "../../../store/actions/authActions";

const SignInLinks = ({ signOut, profile }) => (
  <ul className="right">
    <li>
      <NavLink to="/create">New Project</NavLink>
    </li>
    <li>
      <a onClick={signOut}>Log Out</a>
    </li>
    <li>
      <NavLink to="/" className="btn btn-floating pink lighten-1">
        {(profile && profile.initials) || "XX"}
      </NavLink>
    </li>
  </ul>
);

SignInLinks.propTypes = {
  profile: PropTypes.object,
  signOut: PropTypes.func
};

export default connect(
  null,
  { signOut }
)(SignInLinks);
