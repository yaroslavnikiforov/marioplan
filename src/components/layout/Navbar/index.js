import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import SignedInLinks from "../SignedInLinks";
import SignedOutLinks from "../SignedOutLinks";

const Navbar = ({ auth, profile }) => {
  const links = auth.isEmpty ? (
    <SignedOutLinks />
  ) : (
    <SignedInLinks profile={profile} />
  );

  return (
    <nav className="nav-wrapper grey darken-3">
      <div className="container">
        <Link to="/" className="brand-log">
          Mario Plan
        </Link>
        {links}
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  auth: PropTypes.object,
  profile: PropTypes.object
};

const mapStateToProps = state => ({
  auth: state.firebase.auth,
  profile: state.firebase.profile
});

export default connect(mapStateToProps)(Navbar);
