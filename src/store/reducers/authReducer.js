import { types } from "../actions/types";

const initialState = {
  authError: null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_ERROR:
      console.log("LOGIN_ERROR");
      return { ...state, authError: action.error.message };

    case types.LOGIN_SUCCESS:
      console.log("LOGIN_SUCCESS");
      return { ...state, authError: null };

    case types.SIGNOUT_SUCCESS:
      console.log("SIGNOUT_SUCCESS");
      return state;

    case types.SIGNUP_SUCCESS:
      console.log("SIGNUP_SUCCESS");
      return { ...state, authError: null };

    case types.SIGNUP_ERROR:
      console.log("SIGNUP_ERROR");
      return { ...state, authError: action.error.message };

    default:
      return state;
  }
};

export default authReducer;
