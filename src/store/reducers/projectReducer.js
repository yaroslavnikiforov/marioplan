import { types } from "../actions/types";

const initialState = {
  projects: [
    { id: 1, title: "Help me find peach", content: "blah blah blah" },
    { id: 2, title: "Collect all the starts", content: "blah blah blah" },
    { id: 3, title: "Egg hunt with majin", content: "blah blah blah" }
  ]
};

const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_PROJECT:
      console.info("create project: ", action.project);
      return state;

    case types.CREATE_PROJECT_ERROR:
      console.info("create project error: ", action.err);
      return state;

    default:
      return state;
  }
};

export default projectReducer;
