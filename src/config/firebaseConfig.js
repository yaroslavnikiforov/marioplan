import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

var config = {
  apiKey: "AIzaSyDMiv1fb9t9SeUmTIejwumGDVys5bNILo8",
  authDomain: "mario-plan-98e3a.firebaseapp.com",
  databaseURL: "https://mario-plan-98e3a.firebaseio.com",
  projectId: "mario-plan-98e3a",
  storageBucket: "mario-plan-98e3a.appspot.com",
  messagingSenderId: "657820382947"
};
firebase.initializeApp(config);
firebase.firestore();

export default firebase;
